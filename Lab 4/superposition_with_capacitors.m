D = load('vdc.txt');
C = load('vac.txt');
time = C(:,1);
DC = D(:,2);
AC = C(:,2);
sum = AC + DC;
grid on
xlabel('Time (s)')
ylabel('Vout (V)')
hold on
plot(time,sum,'r',time, AC, 'b', time, DC, 'g')
title('Proving Superposition using Oscilloscope data')
legend('Vad + Vdc','Vac','Vdc')