%clear all 
%close all
clc

f=10:100:10*10^6;
w=2*pi.*f;
R=1000;
C1=0.001*10^-6;
C2=0.1*10^-6;
GLow=sqrt((1./(1+w.^2*R^2*C1^2)).^2+(-w.*R*C1./(1+w.^2*R^2*C1^2)).^2);
GHi=w*R*C2./sqrt(1+w.^2*R^2*C2^2);
semilogx(f,GLow,f,GHi,'r')
set(gca,'Ytick',[0:0.1:1.5])
xlabel('frequency (10Hz:100:10MHz): Hz'), ylabel('Voltage Gain: volts')
title('Frequency Response: Low pass RC: Voltage Gain vs. frequency')
legend('GLow','GHi')


phaseLow=atand(-w*R*C1);
phaseHi=atand(1./(w*R*C2));
figure
semilogx(f,phaseLow, f, phaseHi, 'r')
xlabel('frequency: Hz'), ylabel('phase angle: degrees')
title('Frequency Response: phase angle as a function of frequency')
legend('phaseLow','phaseHi')


