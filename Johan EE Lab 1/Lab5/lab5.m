 
f=10:10:1000000;
y1= (1./(2.*pi.*f.*i)) ./ (1./(2.*pi.*f.*i) + 1500);
y2= 1./y1;
loglog(f,y1,f,y2)
%%plot(f,y1,f,y2)
grid on

%%
R =1500;
C=0.0000001
Vin=linspace(0,1,1000000);
t=linspace(0,(R.*C),1000000);
Vout1=Vin.*(1 - exp(-t./R.*C));
Vout2=Vin.*exp(-t./R.*C);
plot(Vout2,t)
 
%%
%%calculated = load('calculated.txt');
measured1 = load('labview1.txt');
measured2 = load('labview2.txt')

VinHP=measured2(:,2);
VoutHP=measured2(:,3);
gainHP=VoutHP./VinHP;

VinLP=measured1(:,2);
VoutLP=measured1(:,3);
gainLP=VoutLP./VinLP;

f = linspace(50,2000000,40);
freq_meas = measured1(:,1);
hi_calc= (1./(2.*pi.*f.*i)) ./ (1./(2.*pi.*f.*i) + 1500);
low_calc= (1./(2.*pi.*f.*i) + 1500)  ./ (1./(2.*pi.*f.*i));

loglog(f, hi_calc,'r', freq_meas, gainHP,'.')
%%loglog(f, low_calc,'r', freq_meas, gainLP,'.')

grid on;

%%plot(Vs, VR1, 'r', Vs, VR2, '.')
%%xlabel('Voltage Source')
%%ylabel('Resistor Voltage')
%%title('Graph 1 - Theoretical Voltages across resistors')
%%grid on
%%legend('V(R1)','(V(R2)')

%%
%band pass plotting
bandpass = load('bandpassMultisim.txt');
vin=bandpass(:,1)
vout=bandpass(:,2)
frq=linspace(10,10000000,91);
loglog(frq,vout./vin)
grid on

%%
%transient analysis;
hp=load('hpass.txt');
lp=load('lpass.txt');
trans1 = load('transient1.txt');
trans2=load('transient2.txt');
input1 = trans1(:,1);
input1a=lp(:,1);

input2=trans2(:,1);
input2a=hp(:,1);

output1=trans1(:,2);
output1a=lp(:,2);

output2=trans2(:,2);
output2a=hp(:,2);

plot(input1,output1, input1a, output1a)
%%plot(input2,output2, input2a, output2a)

grid on;



%%
RC = 0.005000000000 / (log(0.080000000000/1.040000000000))









