VIinM = load('VS_IVS_Measured.txt');
IR2inM = load('VS_IR2_Measured.txt');
IR3inM = load('VS_IR3_Measured.txt');
VIin = load('VS_IVS.txt');
IR2in = load('VS_IR2.txt');
IR3in = load('VS_IR3.txt');

IVSM = VIinM(:,2)/1000
IVS = VIin(:,2) %this is IVS the input current
IR2 = IR2in(:,2) %this is IR2
IR2M = IR2inM(:,2)/1000 %this is IR2 Measured
IR3 = IR3in(:,2) %this is IR3
IR3M = IR3inM(:,2)/1000 %this is IR3 Measured
SumR2R3 = IR2 + IR3;
SumR2R3M = IR2M + IR3M

plot(IVS,IR2, 'b',  IVS,IR3,'.', IVSM,IR2M,'go', IVSM,IR3M, 'ro', IVS, SumR2R3, '-', IVSM, SumR2R3M, 'bo')
xlabel('Current Source');
ylabel('Resistor Current');
title('Graph 2 - Theoretical Current Across Resistors');
grid on;
legend('I(R2)','I(R3)', 'I(R2) Measured', 'I(R3) Measured', 'I(R2) + I(R3)', 'I(R2) + I(R3) Measured' );

