V = load('VS.txt');
R1 = load('R1.txt');
Vs = V(:,1); %this is the input voltage
VR1 = R1(:,1); %this is Vr1
VR2 = Vs - VR1; %this is Vr2
plot(Vs, VR1, 'r', Vs, VR2, '.')
xlabel('Voltage Source')
ylabel('Resistor Voltage')
title('Graph 1 - Theoretical Voltages across resistors')
grid on
legend('V(R1)','(V(R2)')

