R2 = load('R2.txt');
R1 = load('R1.txt');
Vs = R2(:,1); %this is the input voltage
VR2 = R2(:,2); %this is Vr2 theoretical
VR1 = Vs - VR2; %this is Vr1 theoretical
VsM =  R1(:,1);
VR1M = R1(:,2);
VR2M = VsM - VR1M; 
plot(Vs, VR1, 'r', Vs, VR2, '.', VsM, VR1M, 'b', VsM, VR2M, ':')
xlabel('Voltage Source')
ylabel('Resistor Voltage')
title('Theoretical and Measured Voltages across resistors')
grid on
legend('V(R1)','V(R2)', 'VMeasured(R1)', 'VMeasured(R2)')

