VIin = load('VS_IVS.txt');
IR2in = load('VS_IR2.txt');
IR3in = load('VS_IR3.txt');

IVS = VIin(:,2); %this is IVS the input current
IR2 = IR2in(:,2); %this is IR2
IR3 = IR3in(:,2); %this is IR3
IR2_IR3 = IR2 + IR3;

plot(IVS, IR2_IR3)
xlabel('Current Source - IVS')
ylabel('Total Resistor Current (IR2 + IR3)')
title('Graph 3 - IR2 + IR3 vs IVS')
grid on
legend('I(R2 + R3)')

