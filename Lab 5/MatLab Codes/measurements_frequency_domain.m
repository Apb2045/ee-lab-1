clc;
clear all;
Volt1 = load('frequency_domain_low_frequency_100.txt');
Volt2 = load('frequency_domain_low_frequency_300.txt');
Volt3 = load('frequency_domain_low_frequency_1k.txt');
Volt4 = load('frequency_domain_low_frequency_10k.txt');
Volt5 = load('frequency_domain_low_frequency_100k.txt');
Volt6 = load('frequency_domain_low_frequency_300k.txt');
Volt7 = load('frequency_domain_low_frequency_1M.txt');

in_1 = Volt1(:,2); 
out_1=Volt1(:,3);
gain1=abs(max(out_1)/max(in_1));

in_2 = Volt2(:,2); 
out_2=Volt2(:,3);
gain2=abs(max(out_2)/max(in_2));

in_3 = Volt3(:,2); 
out_3=Volt3(:,3);
gain3=abs(max(out_3)/max(in_3));

in_4 = Volt4(:,2); 
out_4=Volt4(:,3);
gain4=abs(max(out_4)/max(in_4));

in_5 = Volt5(:,2); 
out_5=Volt5(:,3);
gain5=abs(max(out_5)./max(in_5));

in_6 = Volt6(:,2); 
out_6=Volt6(:,3);
gain6=abs(max(out_6)./max(in_6));

in_7 = Volt7(:,2); 
out_7=Volt7(:,3);
gain7=abs(max(out_6)./max(in_6));

freqs = [100 300 1000 10000 100000 300000 1000000]

gains = [gain1 gain2 gain3 gain4 gain5 gain6 gain7]

loglog(freqs,gains,'o')

syms R w C
R=1.5*10^3;
C=0.1*10^(-6);
f=10:10:10^6;
w=2.*pi.*f;
magnitude3=1./(sqrt(1+(R.*w.*C).^2));
magnitude4=R./(sqrt(R.^2+1./(w.*C).^2));
loglog(f,magnitude3,'r--')
hold on
xlabel('freq')
ylabel('gain')
legend('Measured values','Calculated values')
grid on
ylim([10^(-3) 1.2])
title('gain vs freqency of LPF')