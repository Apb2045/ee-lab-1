clc;
clear all;

V1 = load('Figure4c_textVin.txt'); %simulated
V2 = load('Figure4c_textVout.txt'); %simulated
V3 = load('frequeny_response_sweep_band.txt'); %measured

f1 = V1(:,1); 
f3 = V3(:,1)

Vin = V1(:,2); 
Vout = V2(:,2); 

gainSim=Vout./Vin;
gainMeas=V3(:,2);

loglog(f1,gainSim,'r')
hold on
loglog(f3,gainMeas,'b');
hold on
xlabel('freq')
ylabel('gain')
legend('Simulated values','Measured values')
grid on
xlim([10 2*10^6])
ylim([10^(-3) 2])
title('gain for band-pass filter')