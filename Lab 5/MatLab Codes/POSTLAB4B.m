V3 = load('HIGH12RCVIN.txt');
V4 = load('HIGH12RCVOUT.txt');

timein = V3(:,1);
Vin = V3(:,2);
Vout= V4(:,2);

LnVinVout = log(Vin./Vout);

plot(LnVinVout,timein)

xlabel('ln(Vin/Vout)');
ylabel('time');
title('Graph to calculate RC');

