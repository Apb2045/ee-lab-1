%Figure 1 - Low Pass Filter
f = 10:10:10e6;
w = f.*2*pi;
c = .001e-6;
r = 1500;
gain = sqrt(1./(1+w.^2*r^2*c^2));
loglog(f, gain, 'r')
xlabel('freq')
ylabel('gain')
grid on 
hold on
 
%High pass filter
f2 = 10:10:10e6;
w2 = 2*pi.*f2;
c2 = .1e-6;
gain2 = (w2.*r.*c2)./sqrt(1+(w2.^2).*(r^2).*(c2^2));
loglog(f2,gain2, 'b')
title('low and high pass filter gain vs freq')
legend('low pass', 'high pass')