%Figure 1 - Low Pass Filter
clc;
clear all;

c = .001e-6;
r = 1500;

r*c

t = 0:.00000001:r*c

Vlow = 1-exp(-t/(r*c))
Vhigh = exp(-t/(r*c))


plot(t,Vlow);
plot(t,Vhigh);
xlabel('time')
ylabel('V out')
grid on 
hold on
 

