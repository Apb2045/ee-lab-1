clear all
syms R t C
R=1.5*10^3;
C=0.1*10^(-6);
t=0:10^(-6):10^(-3);
Vout1=1.*(1.-exp((-t)./(R.*C)));
Vout2=1.*(exp((-t)./(R.*C)));
plot(t,Vout1)
hold on
plot(t,Vout2)
hold on
xlabel('time')
ylabel('V out')
legend('Low-Pass Filter','High Pass Filter')
grid on
ylim([10.^(-2) 1])
title('Vout vs time for LPF and HPF')