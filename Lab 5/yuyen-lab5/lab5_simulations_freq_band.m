Volt1 = load('Figure4cVin_frequency_text.txt');
Volt2 = load('Figure4cVout_frequency.txt');
f1 = Volt1(:,1); 
Vin = Volt1(:,2); 
f2 = Volt2(:,1); 
Vout = Volt2(:,2); 
gain=Vout./Vin;
loglog(f1,gain,'r')
hold on
xlabel('Frequency(Hz)')
ylabel('magnitude of gain')
legend('band pass filter')
grid on
xlim([10 10^6])
ylim([10^(-3) 2])
title('The magnitude of gain vs frequency for band-pass filter')