Voltage = load('superpostion_capacitor_vout.txt');
Vdc = load('vdc.txt');
Vac = load('vac.txt');
t1 = Voltage(:,1); 
Vout = Voltage(:,2);
t2 = Vdc(:,1);
Vdc1 = Vdc(:,2);
t3 = Vac(:,1);
Vac1 = Vac(:,2);
sum = Vdc1 + Vac1;
plot(t3,Vac1,'r')
hold on
plot(t2,Vdc1,'b')
hold on
plot(t1,sum,'r--')
grid on
xlabel('Time (s)')
ylabel('Voltage (V)')
title('Circuit 2: Superpostion principle verification')
legend('Vac','Vdc','Vac + Vdc')