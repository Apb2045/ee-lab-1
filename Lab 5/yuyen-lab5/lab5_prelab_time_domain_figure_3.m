clear all
syms R w C
R=1.5*10^3;
C=0.1*10^(-6);
f=10:10:10^6;
w=2.*pi.*f;
magnitude3=1./(sqrt(1+(R.*w.*C).^2));
magnitude4=R./(sqrt(R.^2+1./(w.*C).^2));
loglog(f,magnitude3,'r--')
hold on
loglog(f,magnitude4,'b')
hold on
xlabel('Frequency(Hz)')
ylabel('magnitude of gain')
legend('low pass filter','high pass filter')
grid on
ylim([10.^(-2) 1.2])
title('The magnitude of gain vs frequency for band-pass filter')