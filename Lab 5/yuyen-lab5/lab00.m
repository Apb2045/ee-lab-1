V = load('superpostion_capacitor_vout.txt');
D = load('vdc.txt');
C = load('vac.txt');
time1 = V(:,1); 
Vout = V(:,2);
time2 = D(:,1);
DC = D(:,2);
time3 = C(:,1);
AC = C(:,2);
sum = AC + DC;
plot(time1,Vout)
grid on
xlabel('Time (s)')
ylabel('Vout (V)')
hold on
plot(time1,sum,'r')
title('Proving Superposition using Oscilloscope data')
legend('Vout','Putting together AC + DC')