capacitor_measured = load('capacitor_measured.txt');
capacitor_simulation = load('capacitor_simulation.txt');
input_simulation = load('input_simulation.txt');

capacitor_measured_time = capacitor_measured(:,1); 
V_capacitor_measured_input = capacitor_measured(:,2); 
V_capacitor_measured_output= capacitor_measured(:,3); 

OVMAX = max(V_capacitor_measured_output)
IVMAX = max(V_capacitor_measured_input)

capacitor_simulation_time = capacitor_simulation(:,1); 
V_capacitor_simulation = capacitor_simulation(:,2); 

input_simulation_time = input_simulation(:,1); 
V_input_simulation = input_simulation(:,2); 

subplot(2,1,1),plot(capacitor_measured_time,V_capacitor_measured_input,'b--',capacitor_measured_time,V_capacitor_measured_output,'r')

xlabel('Time')
ylabel('Voltage')
title('RC Circuit - LabVIEW')
grid on
legend('Input Voltage', 'Output Volatge')

subplot(2,1,2),plot(input_simulation_time,V_input_simulation,'b--',capacitor_simulation_time,V_capacitor_simulation,'r')

xlabel('Time')
ylabel('Voltage')
title('RC circuit - MultiSim')
grid on
legend('Input Voltage', 'Output Voltage')