V = load('Ivs_data.txt');
D = load('Iv2_data.txt');
C = load('Iv3_data.txt');
Ivs = V(:,2);
Iv2 = D(:,2);
Iv3 = C(:,2);
plot(Ivs, Iv2, 'r', Ivs, Iv3, 'b')
grid on
xlabel('Input Current')
ylabel('Output Current')
title('Graph2 - Output currents vs Input Current')
legend('I(V2)','I(V3)')