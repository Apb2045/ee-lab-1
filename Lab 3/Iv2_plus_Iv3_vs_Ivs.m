V = load('Ivs_data.txt');
D = load('Iv2_data.txt');
C = load('Iv3_data.txt');
Iv1 = V(:,2);
Iv2 = D(:,2);
Iv3 = C(:,2);
x = Iv2 + Iv3;
plot(Iv1, x);
grid on
xlabel('I1')
ylabel('Sum of I2 and I3')
title('Graph7 - Demonstration of KCL for I2 + I3')