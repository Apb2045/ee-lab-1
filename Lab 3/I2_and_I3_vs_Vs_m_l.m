V_input = load('v1_data.txt');
Iv2_l = load('Iv2_data.txt');
Iv3_l = load('Iv3_data.txt');
Iv2_m = load('Iv2_data_m.txt');
Iv3_m = load('Iv3_data_m.txt');
Vs = V_input(:,2);
mIv2 = Iv2_l(:,2);
mIv3 = Iv3_l(:,2);
lIv2 = Iv2_l(:,2);
lIv3 = Iv3_l(:,2);
plot(Vs, lIv2, 'g', Vs, lIv3, 'y', Vs, mIv2, '--r', Vs, mIv3, '--b')
grid on
xlabel('Voltage Source')
ylabel('I2 and I3 MultiSim/LabView')
title('Graph6 - I2 and I3 vs Vs')
legend('I(V2)-l','I(V3)-l','I(V2)-m','I(V3)-m')