VS_vs_R1 = load('resistor1_data.txt');
VS_vs_R2 = load('resistor2_data.txt');
VS = VS_vs_R1(:,1);
VR1 = VS_vs_R1(:,2);
VR2 = VS_vs_R2(:,2);
sum = VR1 + VR2;
plot(VS, sum)
grid on
xlabel('Voltage Source')
ylabel('V1 + V2')
title('Graph5 - Sum of V1 and V2 Voltage')