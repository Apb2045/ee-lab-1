VS_vs_R1 = load('resistor1_data.txt');
VS_vs_R2 = load('resistor2_data.txt');
VS = VS_vs_R1(:,1); %this is the input voltage
VR1 = VS_vs_R1(:,2); %this is Vr1
VR2 = VS_vs_R2(:,2); %this is Vr2
plot(VS, VR1, 'r', Vs, VR2, 'b')
xlabel('Voltage Source')
ylabel('Voltage Across Resistor')
title('Graph 1 - Theoretical Voltages Across Resistors')
grid on
legend('V(R1)','V(R2)')