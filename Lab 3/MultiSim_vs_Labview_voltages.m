LVS_vs_R1 = load('resistor1_data.txt');
LVS_vs_R2 = load('resistor2_data.txt');
MVS_vs_R1 = load('output_node_data.txt');

VS = LVS_vs_R1(:,1);
LVR1 = LVS_vs_R1(:,2);
LVR2 = LVS_vs_R2(:,2);
MVR1 = MVS_vs_R1(:,2);
MVR2 = MVS_vs_R1(:,1) - MVS_vs_R1(:,2);

plot(VS, LVR1, 'r', VS, LVR2, '--r', VS, MVR1, 'b', VS, MVR2, '--b')
grid on
xlabel('Voltage Source')
ylabel('MultiSim and Labview Voltages')
title('Graph4 - MultiSim vs Labview voltages Vr1, Vr2 vs Vs)')
legend('LV(R1)','LV(R2)','MV(R1)','MV(R2)')